import './date/date-prototype';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { OverviewComponent } from './overview/overview.component';
import { PayPeriodsComponent } from './pay-periods/pay-periods.component';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    ExpensesComponent,
    PayPeriodsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
