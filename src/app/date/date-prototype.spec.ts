import './date-prototype';

describe('Date Prototype', () => {
  describe('Dates', () => {
    it('should add days correctly', () => {
      const testDate = new Date('12/29/2018');

      expect(testDate.addDays(2)).toEqual(new Date('12/31/2018'));
      expect(testDate.addDays(3)).toEqual(new Date('1/1/2019'));
      expect(testDate.addDays(-28)).toEqual(new Date('12/1/2018'));
      expect(testDate.addDays(-29)).toEqual(new Date('11/30/2018'));
    });

    it('should add months correctly', () => {
      const testDate = new Date('12/31/2018');

      expect(testDate.addMonths(1)).toEqual(new Date('1/31/2019'));
      expect(testDate.addMonths(2)).toEqual(new Date('3/3/2019'));
    });

    it('should get last day of the month', () => {
      expect(new Date('1/1/2019').getLastDateOfMonth()).toEqual(new Date('1/31/2019'));
      expect(new Date('2/1/2019').getLastDateOfMonth()).toEqual(new Date('2/28/2019'));
      expect(new Date('9/11/2019').getLastDateOfMonth()).toEqual(new Date('9/30/2019'));
    });

    it('should get difference in days', () => {
      const date1 = new Date('1/1/2018');
      const date2 = new Date('1/5/2018');
      const date3 = new Date('1/1/2019');
      const date4 = new Date('6/7/2019');

      expect(date1.differenceInDays(date2, false)).toEqual(4);
      expect(date1.differenceInDays(date2, true)).toEqual(5);
      expect(date2.differenceInDays(date1, false)).toEqual(-4);
      expect(date2.differenceInDays(date1, true)).toEqual(-5);
      expect(date1.differenceInDays(date3, false)).toEqual(365);
      expect(date2.differenceInDays(date4, false)).toEqual(518);
      expect(date1.differenceInDays(date1, false)).toEqual(0);
      expect(date1.differenceInDays(date1, true)).toEqual(0);
      expect(date1.differenceInDays(new Date('1/2/2018'), false)).toEqual(1);
      expect(date1.differenceInDays(new Date('1/2/2018'), true)).toEqual(2);
    });

    it('should get difference in weeks', () => {
      const startDate = new Date('1/1/2020');
      const endDate = new Date('2/1/2020');

      expect(startDate.differenceInWeeks(endDate)).toEqual(4);
      expect(startDate.differenceInWeeks(new Date('1/2/2020'))).toEqual(0);
      expect(new Date('1/2/2020').differenceInWeeks(new Date('1/23/2020'))).toEqual(3);
      expect(new Date('1/3/2020').differenceInWeeks(new Date('1/9/2020'))).toEqual(0);
      expect(new Date('1/3/2020').differenceInWeeks(new Date('1/10/2020'))).toEqual(1);
      expect(new Date('1/3/2020').differenceInWeeks(new Date('1/11/2020'))).toEqual(1);
    });

    it('should get difference in months', () => {
      const startDate1 = new Date('1/1/2020');
      const startDate2 = new Date('12/1/2020');

      expect(startDate1.differenceInMonths(new Date('2/1/2020'), false)).toEqual(1);
      expect(startDate1.differenceInMonths(new Date('2/1/2020'), true)).toEqual(2);
      expect(startDate1.differenceInMonths(new Date('2/3/2020'), false)).toEqual(1);
      expect(startDate1.differenceInMonths(new Date('2/3/2020'), true)).toEqual(2);
      expect(startDate1.differenceInMonths(new Date('4/3/2020'), false)).toEqual(3);
      expect(startDate1.differenceInMonths(new Date('4/3/2020'), true)).toEqual(4);
      expect(startDate1.differenceInMonths(new Date('1/1/2021'), false)).toEqual(12);
      expect(startDate1.differenceInMonths(new Date('1/1/2021'), true)).toEqual(13);
      expect(startDate1.differenceInMonths(new Date('12/1/2019'), false)).toEqual(-1);
      expect(startDate1.differenceInMonths(new Date('12/1/2019'), true)).toEqual(-2);

      expect(startDate2.differenceInMonths(new Date('1/1/2021'), false)).toEqual(1);
      expect(startDate2.differenceInMonths(new Date('1/1/2021'), true)).toEqual(2);
    });

    it('should get difference in years', () => {
      const startDate1 = new Date('1/2/2020');

      expect(startDate1.differenceInYears(new Date('1/2/2021'))).toEqual(1);
      expect(startDate1.differenceInYears(new Date('1/1/2021'))).toEqual(0);
      expect(startDate1.differenceInYears(new Date('1/2/2022'))).toEqual(2);
      expect(startDate1.differenceInYears(new Date('1/1/2022'))).toEqual(1);

      expect(startDate1.differenceInYears(new Date('1/2/2019'))).toEqual(-1);
      expect(startDate1.differenceInYears(new Date('1/1/2019'))).toEqual(-1);
      expect(startDate1.differenceInYears(new Date('1/2/2018'))).toEqual(-2);
      expect(startDate1.differenceInYears(new Date('1/1/2018'))).toEqual(-2);
    });
  });
});
