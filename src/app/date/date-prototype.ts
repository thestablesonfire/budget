Date.prototype.addDays = addDays;
Date.prototype.addMonths = addMonths;
Date.prototype.addOrSubtractIfCountingFirst = addOrSubtractIfCountingFirst;
Date.prototype.addYears = addYears;
Date.prototype.differenceInDays = differenceInDays;
Date.prototype.differenceInMonths = differenceInMonths;
Date.prototype.differenceInWeeks = differenceInWeeks;
Date.prototype.differenceInYears = differenceInYears;
Date.prototype.getLastDateOfMonth = getLastDateOfMonth;
Date.prototype.getDateDiff = getDateDiff;

interface Date {
  addDays: typeof addDays;
  addMonths: typeof addMonths;
  addOrSubtractIfCountingFirst: typeof addOrSubtractIfCountingFirst;
  addYears: typeof addYears;
  differenceInDays: typeof differenceInDays;
  differenceInMonths: typeof differenceInMonths;
  differenceInWeeks: typeof differenceInWeeks;
  differenceInYears: typeof differenceInYears;
  getLastDateOfMonth: typeof getLastDateOfMonth;
  getDateDiff: typeof getDateDiff;
}

function addDays(days: number): Date {
  const date = new Date(this.valueOf());

  date.setDate(date.getDate() + days);

  return date;
};

function addMonths(months: number): Date {
  const date = new Date(this.valueOf());

  date.setMonth(date.getMonth() + months);

  return date;
};

function addOrSubtractIfCountingFirst(diff: number): number {
  if (diff) {
    if (diff > 0) {
      diff += 1;
    } else {
      diff -= 1;
    }
  }

  return diff;
};

function addYears(numYears: number): Date {
  const date = new Date(this.valueOf());

  date.setFullYear(this.getFullYear() + numYears);

  return date;
};

function differenceInDays(diffDate: Date, countFirstDay: boolean): number {
  const daysConst = 1000 * 60 * 60 * 24;
  const daysDiff = this.getDateDiff(diffDate, daysConst);

  return countFirstDay
    ? this.addOrSubtractIfCountingFirst(daysDiff)
    : daysDiff;
};

function differenceInMonths(diffDate: Date, countFirstMonth: boolean): number {
  const monthDiff = diffDate.getMonth() - this.getMonth();
  const yearDiff = diffDate.getFullYear() - this.getFullYear();
  const diffMonths = yearDiff * 12 + monthDiff;

  return countFirstMonth
    ? this.addOrSubtractIfCountingFirst(diffMonths)
    : diffMonths;
};

function differenceInWeeks(diffDate: Date): number {
  const weeksConst = 1000 * 60 * 60 * 24 * 7;

  return this.getDateDiff(diffDate, weeksConst);
};

function differenceInYears(diffDate: Date): number {
  const compareDate = new Date(diffDate.valueOf());
  let yearsDiff = diffDate.getFullYear() - this.getFullYear();

  compareDate.setFullYear(this.getFullYear());

  if (yearsDiff >= 0 && compareDate.getTime() < this.getTime()) {
    yearsDiff -= 1;
  }

  if (yearsDiff < 0 && compareDate.getTime() > this.getTime()) {
    yearsDiff += 1;
  }

  return yearsDiff;
};

function getLastDateOfMonth(): Date {
  const date = new Date(this.valueOf());

  date.setFullYear(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  );
  date.addDays(-1);

  return date;
};

function getDateDiff(diffDate: Date, unitConst: number): number {
  const thisDate = new Date(this.valueOf());

  const thisUTC = Date.UTC(
    thisDate.getFullYear(),
    thisDate.getMonth(),
    thisDate.getDate()
  );
  const diffUTC = Date.UTC(
    diffDate.getFullYear(),
    diffDate.getMonth(),
    diffDate.getDate()
  );

  return Math.floor((diffUTC - thisUTC) / (unitConst));
}