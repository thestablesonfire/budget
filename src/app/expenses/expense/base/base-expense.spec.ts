import { BaseInterval } from '../../../interval/base-interval';
import { IntervalType } from '../../../interval/types/interval-type.enum';
import { TransactionType } from '../../../types/transaction-type.enum';
import { BaseExpense } from './base-expense';

describe('BaseExpense', () => {
  it('should create an instance', () => {
    expect(new BaseExpense(
      'Coffee',
      5,
      new Date('1/1/2019'),
      new BaseInterval(1),
      TransactionType.Fixed,
    )).toBeTruthy();
  });

  it('should get values correctly', () => {
    const expense = new BaseExpense(
      'Coffee',
      5,
      new Date('1/1/2019'),
      new BaseInterval(1),
      TransactionType.Fixed,
    );

    expect(expense.getName()).toBe('Coffee');
    expect(expense.getValue()).toBe(5);
    expect(expense.getDueDate()).toEqual(new Date('1/1/2019'));
    expect(expense.getInterval()).toEqual(new BaseInterval(1));
    expect(expense.getCostType()).toBe(TransactionType.Fixed);
    expect(expense.getExpensesForPeriod(null, null)).toBe(5);
  });
});
