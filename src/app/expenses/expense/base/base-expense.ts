import { BaseInterval } from 'src/app/interval/base-interval';
import { TransactionType } from 'src/app/types/transaction-type.enum';

export class BaseExpense {
  constructor(
    protected name: string,
    protected value: number,
    protected dueDate: Date,
    protected interval: BaseInterval,
    protected costType: TransactionType,
  ) { }

  getName(): string {
    return this.name;
  }

  getValue(): number {
    return this.value;
  }

  getDueDate(): Date {
    return this.dueDate;
  }

  getInterval(): BaseInterval {
    return this.interval;
  }

  getCostType(): TransactionType {
    return this.costType;
  }

  getExpensesForPeriod(startDate: Date, endDate: Date): number {
    return this.value * this.interval.getNumOccurrences(
      this.dueDate,
      startDate,
      endDate
    );
  }
}
