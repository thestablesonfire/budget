import { BaseInterval } from '../../../interval/base-interval';
import { DailyInterval } from '../../../interval/daily/daily-interval';
import { TransactionType } from '../../../types/transaction-type.enum';
import { DailyExpense } from './daily-expense';

describe('DailyExpense', () => {
  it('should create an instance', () => {
    expect(new DailyExpense(
      'Coffee',
      5,
      new Date('1/1/2019'),
      new BaseInterval(1),
      TransactionType.Fixed
    )).toBeTruthy();
  });

  it('should calculate expenses correctly', () => {
    const expense = new DailyExpense(
      'Coffee',
      5,
      new Date('1/1/2019'),
      new DailyInterval(1),
      TransactionType.Fixed
    );

    expect(expense.getExpensesForPeriod(
      new Date('1/1/2018'),
      new Date('1/5/2018')
    )).toEqual(25);

    expect(expense.getExpensesForPeriod(
      new Date('1/5/2018'),
      new Date('1/1/2018')
    )).toEqual(0);
  });
});
