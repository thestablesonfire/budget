import { BaseInterval } from '../../../interval/base-interval';
import { IntervalType } from '../../../interval/types/interval-type.enum';
import { TransactionType } from '../../../types/transaction-type.enum';
import { BaseExpense } from '../base/base-expense';

export class DailyExpense extends BaseExpense {
  constructor(
    name: string,
    value: number,
    dueDate: Date,
    interval: BaseInterval,
    costType: TransactionType
  ) {
    super(name, value, dueDate, interval, costType);
  }

  getExpensesForPeriod(startDate: Date, endDate: Date): number {
    return startDate.valueOf() < endDate.valueOf()
      ? this.value * this.interval.getNumOccurrences(this.dueDate, startDate, endDate)
      : 0;
  }
}
