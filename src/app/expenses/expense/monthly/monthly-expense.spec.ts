import { MonthlyOnDateInterval } from '../../../interval/monthly-on-date/monthly-on-date-interval';
import { IntervalType } from '../../../interval/types/interval-type.enum';
import { TransactionType } from '../../../types/transaction-type.enum';
import { MonthlyExpense } from './monthly-expense';

describe('MonthlyExpense', () => {
  let expense;

  beforeEach(() => {
    expense = new MonthlyExpense(
      'Rent',
      2150.00,
      new Date('1/1/2020'),
      new MonthlyOnDateInterval(),
      TransactionType.Fixed,
    );
  });

  it('should create an instance', () => {
    expect(expense).toBeTruthy();
  });
});
