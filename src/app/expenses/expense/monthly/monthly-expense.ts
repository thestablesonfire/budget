import { BaseInterval } from '../../../interval/base-interval';
import { TransactionType } from '../../../types/transaction-type.enum';
import { BaseExpense } from '../base/base-expense';

export class MonthlyExpense extends BaseExpense {
  constructor(
    name: string,
    value: number,
    dueDate: Date,
    interval: BaseInterval,
    costType: TransactionType,
  ) {
    super(name, value, dueDate, interval, costType);
  }
}
