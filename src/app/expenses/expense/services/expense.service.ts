import { TransactionType } from 'src/app/types/transaction-type.enum';
import { MonthlyOnDateInterval } from 'src/app/interval/monthly-on-date/monthly-on-date-interval';

import { Injectable } from '@angular/core';

import { BaseExpense } from '../base/base-expense';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  constructor() { }

  getExpenses() {
    return ([
      new BaseExpense(
        'Metro Pass',
        0,
        new Date('1/1/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Rent',
        1395,
        new Date('1/1/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Student Loans',
        420,
        new Date('1/2/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Renter\'s Insurance',
        26.88,
        new Date('1/2/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Savings',
        267.50,
        new Date('1/5/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Linode',
        10.00,
        new Date('1/4/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Spotify',
        9.99,
        new Date('1/8/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Netflix',
        12.99,
        new Date('1/9/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Savings',
        267.50,
        new Date('1/20/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Gas Bill',
        20,
        new Date('1/21/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Variable,
      ),
      new BaseExpense(
        'Internet',
        50.55,
        new Date('1/22/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Cell Phone',
        60,
        new Date('1/24/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Fixed,
      ),
      new BaseExpense(
        'Electric Bill',
        100,
        new Date('1/30/2019'),
        new MonthlyOnDateInterval(),
        TransactionType.Variable,
      )
    ]).sort(this.sortExpenses);
  }

  private sortExpenses = (a: BaseExpense, b: BaseExpense): number => {
    let returnVal = b.getDueDate().differenceInDays(a.getDueDate(), true);

    if (!returnVal) {
      returnVal = a.getName().localeCompare(b.getName())
    }

    return returnVal;
  }
}
