import { TestBed } from '@angular/core/testing';

import { DailyInterval } from '../../../../interval/daily/daily-interval';
import {
  MonthlyOnDateInterval
} from '../../../../interval/monthly-on-date/monthly-on-date-interval';
import { IntervalType } from '../../../../interval/types/interval-type.enum';
import { WeeklyDayIndicator } from '../../../../interval/weekly/indicator/weekly-day-indicator';
import { WeeklyInterval } from '../../../../interval/weekly/weekly-interval';
import { TransactionType } from '../../../../types/transaction-type.enum';
import { ExpenseFactoryService } from './expense-factory.service';

let service: ExpenseFactoryService;

describe('ExpenseFactoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ExpenseFactoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create a fixed daily expense', () => {
    const expense = service.getDailyFixedExpense(
      'Coffee',
      5,
      new Date('1/1/2020'),
      1
    );

    expect(expense.getName()).toEqual('Coffee');
    expect(expense.getValue()).toEqual(5);
    expect(expense.getDueDate()).toEqual(new Date('1/1/2020'));
    expect(expense.getInterval()).toEqual(new DailyInterval(1));
    expect(expense.getCostType()).toEqual(TransactionType.Fixed);
    expect(expense.getExpensesForPeriod(
      new Date('1/5/2020'),
      new Date('1/14/2020')
    )).toEqual(50);
  });

  it('should create a fixed weekly expense', () => {
    let expense;
    const indicator = new WeeklyDayIndicator(
      [
        false,
        true,
        true,
        true,
        true,
        true,
        false
      ],
      1
    );

    expense = service.getWeeklyFixedExpense(
      'Coffee',
      4,
      indicator
    );

    expect(expense.getName()).toEqual('Coffee');
    expect(expense.getValue()).toEqual(4);
    expect(expense.getDueDate()).toEqual(null);
    expect(expense.getInterval()).toEqual(new WeeklyInterval(indicator));
    expect(expense.getCostType()).toEqual(TransactionType.Fixed);
    expect(expense.getExpensesForPeriod(
      new Date('1/2/2020'),
      new Date('1/13/2020')
    )).toEqual(32);
  });

  it('should create a fixed monthly-on-number expense', () => {
    const expense = service.getMonthlyOnNumberExpense(
      'Rent',
      2150,
      new Date('1/1/2020'),
      TransactionType.Fixed
    );

    expect(expense.getName()).toEqual('Rent');
    expect(expense.getValue()).toEqual(2150);
    expect(expense.getDueDate()).toEqual(new Date('1/1/2020'));
    expect(expense.getInterval()).toEqual(new MonthlyOnDateInterval());
    expect(expense.getCostType()).toEqual(TransactionType.Fixed);
    expect(expense.getExpensesForPeriod(
      new Date('1/2/2020'),
      new Date('6/1/2020')
    )).toEqual(10750);
  });
});
