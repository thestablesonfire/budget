import { Injectable } from '@angular/core';

import { BaseInterval } from '../../../../interval/base-interval';
import { DailyInterval } from '../../../../interval/daily/daily-interval';
import {
  MonthlyOnDateInterval
} from '../../../../interval/monthly-on-date/monthly-on-date-interval';
import { IntervalType } from '../../../../interval/types/interval-type.enum';
import { WeeklyDayIndicator } from '../../../../interval/weekly/indicator/weekly-day-indicator';
import { WeeklyInterval } from '../../../../interval/weekly/weekly-interval';
import { TransactionType } from '../../../../types/transaction-type.enum';
import { DailyExpense } from '../../daily/daily-expense';
import { MonthlyExpense } from '../../monthly/monthly-expense';
import { WeeklyExpense } from '../../weekly/weekly-expense';

@Injectable({
  providedIn: 'root'
})
export class ExpenseFactoryService {
  constructor() { }

  getDailyFixedExpense(
    name: string,
    cost: number,
    dueDate: Date,
    recurrence: number
  ): DailyExpense {
    return this.getDailyExpense(name, cost, dueDate, recurrence, TransactionType.Fixed);
  }

  getMonthlyOnNumberExpense(
    name: string,
    value: number,
    dueDate: Date,
    costType: TransactionType

  ) {
    return this.getMonthlyExpense(
      name,
      value,
      dueDate,
      new MonthlyOnDateInterval(),
      costType,
      IntervalType.MonthlyOnNumber
    );
  }

  getWeeklyFixedExpense(
    name: string,
    value: number,
    daysIndicator: WeeklyDayIndicator
  ): WeeklyExpense {
    return this.getWeeklyExpense(
      name,
      value,
      daysIndicator,
      TransactionType.Fixed
    );
  }

  private getDailyExpense(
    name: string,
    cost: number,
    dueDate: Date,
    recurrence: number,
    type: TransactionType
  ) {
    return new DailyExpense(
      name,
      cost,
      dueDate,
      new DailyInterval(recurrence),
      type,
    );
  }

  private getMonthlyExpense(
    name: string,
    value: number,
    dueDate: Date,
    interval: BaseInterval,
    costType: TransactionType,
    intervalType: IntervalType
  ) {
    return new MonthlyExpense(
      name,
      value,
      dueDate,
      interval,
      costType,
    );
  }

  private getWeeklyExpense(
    name: string,
    value: number,
    daysIndicator: WeeklyDayIndicator,
    type: TransactionType
  ) {
    return new WeeklyExpense(
      name,
      value,
      new WeeklyInterval(daysIndicator),
      type
    );
  }
}
