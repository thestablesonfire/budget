import { WeeklyDayIndicator } from '../../../interval/weekly/indicator/weekly-day-indicator';
import { WeeklyInterval } from '../../../interval/weekly/weekly-interval';
import { TransactionType } from '../../../types/transaction-type.enum';
import { WeeklyExpense } from './weekly-expense';

describe('WeeklyExpense', () => {
  let expense: WeeklyExpense;

  it('should create an instance', () => {
    expense = new WeeklyExpense(
      'Coffee',
      5,
      new WeeklyInterval(
        new WeeklyDayIndicator(
          [
            false,
            true,
            true,
            true,
            true,
            true,
            false
          ],
          1
        )
      ),
      TransactionType.Fixed
    );

    expect(expense).toBeTruthy();
  });
});
