import { WeeklyInterval } from '../../../interval/weekly/weekly-interval';
import { TransactionType } from '../../../types/transaction-type.enum';
import { BaseExpense } from '../base/base-expense';

export class WeeklyExpense extends BaseExpense {
  constructor(
    name: string,
    value: number,
    interval: WeeklyInterval,
    costType: TransactionType
  ) {
    super(name, value, null, interval, costType);
  }

  getExpensesForPeriod(startDate: Date, endDate: Date) {
    return this.value * this.interval.getNumOccurrences(null, startDate, endDate);
  }
}
