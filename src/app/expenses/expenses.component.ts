import { Component, OnInit } from '@angular/core';

import { BaseExpense } from './expense/base/base-expense';
import { ExpenseService } from './expense/services/expense.service';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.scss']
})
export class ExpensesComponent implements OnInit {
  public expenses: BaseExpense[];

  constructor(private expenseService: ExpenseService) {
    this.expenses = [];
  }

  ngOnInit() {
    this.getExpenses();
  }

  private getExpenses(): void {
    this.expenses = this.expenseService.getExpenses();
  }
}
