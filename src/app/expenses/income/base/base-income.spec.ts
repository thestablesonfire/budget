import { BaseIncome } from './base-income';
import { BaseInterval } from '../../../interval/base-interval';
import { IntervalType } from '../../../interval/types/interval-type.enum';
import { TransactionType } from '../../../types/transaction-type.enum';

describe('BaseIncome', () => {
  it('should create an instance', () => {
    expect(new BaseIncome(
      'paycheck',
      100,
      new Date('1/1/2020'),
      new BaseInterval(IntervalType.Daily),
      TransactionType.Fixed,
    )).toBeTruthy();
  });
});
