import { BaseInterval } from 'src/app/interval/base-interval';
import { TransactionType } from 'src/app/types/transaction-type.enum';
import { BaseExpense } from '../../expense/base/base-expense';

export class BaseIncome extends BaseExpense {
  constructor(
    protected name: string,
    protected value: number,
    protected dueDate: Date,
    protected interval: BaseInterval,
    protected incomeType: TransactionType,
  ) {
    super(name, value, dueDate, interval, incomeType);
  }
}
