import { DailyIncome } from './daily-income';
import { BaseInterval } from '../../../interval/base-interval';
import { TransactionType } from '../../../types/transaction-type.enum';
import { IntervalType } from '../../../interval/types/interval-type.enum';

describe('DailyIncome', () => {
  it('should create an instance', () => {
    expect(new DailyIncome(
      'paycheck',
      100,
      new Date('1/1/2020'),
      new BaseInterval(IntervalType.Daily),
      TransactionType.Fixed,
    )).toBeTruthy();
  });
});
