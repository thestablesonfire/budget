import { DailyExpense } from '../../expense/daily/daily-expense';
import { BaseInterval } from '../../../interval/base-interval';
import { TransactionType } from '../../../types/transaction-type.enum';

export class DailyIncome extends DailyExpense {
  constructor(
    name: string,
    value: number,
    dueDate: Date,
    interval: BaseInterval,
    costType: TransactionType,
  ) {
    super(name, value, dueDate, interval, costType);
  }
}
