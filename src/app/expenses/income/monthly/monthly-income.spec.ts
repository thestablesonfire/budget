import { MonthlyIncome } from './monthly-income';
import { TransactionType } from '../../../types/transaction-type.enum';
import { BaseInterval } from '../../../interval/base-interval';
import { IntervalType } from '../../../interval/types/interval-type.enum';

describe('MonthlyIncome', () => {
  it('should create an instance', () => {
    expect(new MonthlyIncome(
      'paycheck',
      1000,
      new Date('1/1/2020'),
      new BaseInterval(IntervalType.MonthlyOnNumber),
      TransactionType.Fixed,
    )).toBeTruthy();
  });
});
