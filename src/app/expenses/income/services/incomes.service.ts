import { Injectable } from '@angular/core';

import { BaseIncome } from '../base/base-income';
import { TransactionType } from 'src/app/types/transaction-type.enum';
import { WeeklyInterval } from '../../../interval/weekly/weekly-interval';
import { WeeklyDayIndicator } from 'src/app/interval/weekly/indicator/weekly-day-indicator';

@Injectable({
  providedIn: 'root'
})
export class IncomesService {
  constructor() { }

  public getIncomes(): BaseIncome[] {
    return [
      new BaseIncome(
        'Paycheck',
        2003.80,
        new Date('5/8/2020'),
        new WeeklyInterval(
          new WeeklyDayIndicator(
            [ false, false, false, false, false, true, false ],
            2
          )
        ),
        TransactionType.Fixed,
      ),
    ];
  }
}
