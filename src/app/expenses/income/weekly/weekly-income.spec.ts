import { WeeklyIncome } from './weekly-income';
import { WeeklyInterval } from '../../../interval/weekly/weekly-interval';
import { TransactionType } from '../../../types/transaction-type.enum';
import { WeeklyDayIndicator } from '../../../interval/weekly/indicator/weekly-day-indicator';

describe('WeeklyIncome', () => {
  it('should create an instance', () => {
    expect(new WeeklyIncome(
      'paycheck',
      1000,
      new WeeklyInterval(new WeeklyDayIndicator([], 1)),
      TransactionType.Fixed,
    )).toBeTruthy();
  });
});
