import { WeeklyExpense } from '../../expense/weekly/weekly-expense';
import { WeeklyInterval } from '../../../interval/weekly/weekly-interval';
import { TransactionType } from '../../../types/transaction-type.enum';

export class WeeklyIncome extends WeeklyExpense {
  constructor(
    name: string,
    value: number,
    interval: WeeklyInterval,
    costType: TransactionType
  ) {
    super(name, value, interval, costType);
  }
}
