export enum PayPeriodType {
  Weekly = 1,
  BiWeekly,
  SemiMonthly,
  Monthly
}
