import { IntervalType } from './types/interval-type.enum';

export class BaseInterval {
  constructor(protected type: IntervalType) { }

  getNumOccurrences(
    dueDate: Date,
    startDate: Date,
    endDate: Date
  ): number {
    return 1;
  }
}
