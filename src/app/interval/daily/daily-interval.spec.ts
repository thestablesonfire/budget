import { DailyInterval } from './daily-interval';

describe('DailyInterval', () => {
  it('should create an instance', () => {
    expect(new DailyInterval(3)).toBeTruthy();
  });

  it('should calculate occurrences, int: 1', () => {
    const int = new DailyInterval(1);
    expect(int.getNumOccurrences(
      new Date('2/1/2017'),
      new Date('2/1/2018'),
      new Date('2/28/2018'),
    )).toBe(28);

    expect(int.getNumOccurrences(
      new Date('2/1/2018'),
      new Date('2/1/2018'),
      new Date('2/28/2018'),
    )).toBe(28);

    expect(int.getNumOccurrences(
      new Date('2/1/2019'),
      new Date('2/1/2018'),
      new Date('2/28/2018'),
    )).toBe(28);
  });

  it('should calculate occurrences, int: 2', () => {
    const int = new DailyInterval(2);

    expect(int.getNumOccurrences(
      new Date('2/1/2018'),
      new Date('2/1/2018'),
      new Date('2/28/2018'),
    )).toBe(14);

    expect(int.getNumOccurrences(
      new Date('2/4/2018'),
      new Date('1/30/2018'),
      new Date('2/28/2018'),
    )).toBe(15);
  });

  it('should calculate occurrences, int: 3', () => {
    const int = new DailyInterval(3);

    expect(int.getNumOccurrences(
      new Date('1/1/2018'),
      new Date('1/3/2018'),
      new Date('1/13/2018'),
    )).toBe(4);

    expect(int.getNumOccurrences(
      new Date('1/1/2019'),
      new Date('1/3/2018'),
      new Date('1/13/2018'),
    )).toBe(4);
  });

  it('should calculate occurrences, int: 14', () => {
    const int = new DailyInterval(14);

    expect(int.getNumOccurrences(
      new Date('1/1/2018'),
      new Date('1/1/2018'),
      new Date('1/31/2018'),
    )).toBe(3);

    expect(int.getNumOccurrences(
      new Date('1/1/2019'),
      new Date('1/1/2018'),
      new Date('1/31/2018'),
    )).toBe(3);

    expect(int.getNumOccurrences(
      new Date('1/1/2017'),
      new Date('1/1/2018'),
      new Date('1/31/2018'),
    )).toBe(3);
  });
});
