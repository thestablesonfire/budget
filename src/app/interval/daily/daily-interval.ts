import { BaseInterval } from '../base-interval';
import { IntervalType } from '../types/interval-type.enum';

export class DailyInterval extends BaseInterval {
  // numDays = num days until (and including) next occurrence
  constructor(private numDays: number) {
    super(IntervalType.Daily);
  }

  getNumOccurrences(
    dueDate: Date,
    startDate: Date,
    endDate: Date
  ): number {
    // Should be how many days to adjust to get to first occurrence
    let numDiffDays = startDate.differenceInDays(dueDate, false);
    let daysToAdd = 0;
    let startCountDate;

    // If num diff days is negative, that means the due date is before the start date
    if (numDiffDays < 0) {
      numDiffDays *= -1;
      numDiffDays -= 1;
    }

    numDiffDays = numDiffDays % 365;
    daysToAdd = numDiffDays % this.numDays;
    startCountDate = startDate.addDays(daysToAdd);

    return 1 + Math.floor(startCountDate.differenceInDays(endDate, false) / this.numDays);
  }
}
