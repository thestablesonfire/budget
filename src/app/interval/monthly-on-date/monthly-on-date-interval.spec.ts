import { MonthlyOnDateInterval } from './monthly-on-date-interval';

describe('MonthlyOnDateInterval', () => {
  it('should create an instance', () => {
    expect(new MonthlyOnDateInterval()).toBeTruthy();
  });

  it('should get num occurrences', () => {
    const int = new MonthlyOnDateInterval();

    expect(int.getNumOccurrences(
      new Date('1/1/2018'),
      new Date('1/1/2018'),
      new Date('5/1/2018')
    )).toEqual(5);

    expect(int.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2021')
    )).toEqual(13);

    expect(int.getNumOccurrences(
      new Date('1/1/2018'),
      new Date('1/2/2018'),
      new Date('5/1/2018')
    )).toEqual(4);

    expect(int.getNumOccurrences(
      new Date('1/2/2018'),
      new Date('1/1/2018'),
      new Date('5/1/2018')
    )).toEqual(4);

    expect(int.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('3/1/2020'),
      new Date('4/1/2020')
    )).toEqual(2);

    expect(int.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2020')
    )).toEqual(1);

    expect(int.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2021')
    )).toEqual(13);

    expect(int.getNumOccurrences(
      new Date('1/2/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2021')
    )).toEqual(12);

    expect(int.getNumOccurrences(
      new Date('3/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2021')
    )).toEqual(13);
  });

  it('should get correct num occurrences w different due dates', () => {
    const int = new MonthlyOnDateInterval();

    expect(int.getNumOccurrences(
      new Date('1/1/2017'),
      new Date('1/1/2018'),
      new Date('5/1/2018')
    )).toEqual(5);

    expect(int.getNumOccurrences(
      new Date('1/2/2019'),
      new Date('1/1/2018'),
      new Date('5/1/2018')
    )).toEqual(4);
  });
});
