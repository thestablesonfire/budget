import { BaseInterval } from '../base-interval';
import { IntervalType } from '../types/interval-type.enum';

export class MonthlyOnDateInterval extends BaseInterval {
  constructor() {
    super(IntervalType.MonthlyOnNumber);
  }

  getNumOccurrences(
    dueDate: Date,
    startDate: Date,
    endDate: Date
  ): number {
    const dayOfMonth = dueDate.getDate();
    let compareEndDate = new Date(endDate.valueOf());
    let compareStartDate = new Date(startDate.valueOf());
    let occurrences = 0;

    compareStartDate.setDate(dayOfMonth);
    compareEndDate.setDate(dayOfMonth);

    if (compareStartDate.getTime() < startDate.getTime()) {
      compareStartDate = this.addMonthSetDay(compareStartDate, dayOfMonth, 1);
    }

    if (compareEndDate.getTime() > endDate.getTime()) {
      compareEndDate = this.addMonthSetDay(compareEndDate, dayOfMonth, -1);
    }

    while (compareStartDate.getTime() <= compareEndDate.getTime()) {
      occurrences++;
      compareStartDate = this.addMonthSetDay(compareStartDate, dayOfMonth, 1);
    }

    return occurrences;
  }

  private addMonthSetDay(oldDate: Date, dayOfMonth: number, numToAdd: number) {
    const date = new Date(oldDate).addMonths(numToAdd);

    date.setDate(dayOfMonth);

    return date;
  }
}
