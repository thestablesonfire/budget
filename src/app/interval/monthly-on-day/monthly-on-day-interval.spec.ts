import { MonthlyOnDayInterval } from './monthly-on-day-interval';

describe('MonthlyOnDayInterval', () => {
  let interval;

  beforeEach(() => {
    interval = new MonthlyOnDayInterval();
  });

  it('should create an instance', () => {
    expect(new MonthlyOnDayInterval()).toBeTruthy();
  });

  it('should get the correct number of occurrences', () => {
    // second sunday
    expect(interval.getNumOccurrences(
      new Date('1/12/2020'),
      new Date('2/1/2020'),
      new Date('6/1/2020')
    )).toEqual(4);

    // first sunday
    expect(interval.getNumOccurrences(
      new Date('3/1/2020'),
      new Date('3/2/2020'),
      new Date('6/1/2020')
    )).toEqual(2);

    // first monday
    expect(interval.getNumOccurrences(
      new Date('3/2/2020'),
      new Date('3/3/2020'),
      new Date('6/1/2020')
    )).toEqual(3);

    // first wed
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/2/2020'),
      new Date('6/1/2020')
    )).toEqual(4);

    // first thurs
    expect(interval.getNumOccurrences(
      new Date('1/2/2020'),
      new Date('1/1/2020'),
      new Date('6/1/2020')
    )).toEqual(5);

    // first wed
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('12/1/2019'),
      new Date('3/1/2020')
    )).toEqual(3);

    // first wed
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/1/2020'),
      new Date('4/30/2020')
    )).toEqual(4);

    // REVERSE
    // second sunday
    expect(interval.getNumOccurrences(
      new Date('1/12/2020'),
      new Date('6/1/2020'),
      new Date('2/1/2020')
    )).toEqual(4);

    // first sunday
    expect(interval.getNumOccurrences(
      new Date('3/1/2020'),
      new Date('6/1/2020'),
      new Date('3/2/2020')
    )).toEqual(2);

    // first monday
    expect(interval.getNumOccurrences(
      new Date('3/2/2020'),
      new Date('6/1/2020'),
      new Date('3/3/2020')
    )).toEqual(3);

    // first wed
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('6/1/2020'),
      new Date('1/2/2020')
    )).toEqual(4);

    // first thurs
    expect(interval.getNumOccurrences(
      new Date('1/2/2020'),
      new Date('6/1/2020'),
      new Date('1/1/2020')
    )).toEqual(5);

    // first wed
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('3/1/2020'),
      new Date('12/1/2019')
    )).toEqual(3);

    // first wed
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('4/30/2020'),
      new Date('1/1/2020')
    )).toEqual(4);
  });

  it('should test the getDayAndOccurrenceForDate method', () => {
    expect(interval.getNthOccurrenceOfDayForMonth(
      1, 0, new Date('2/1/2020')
    )).toEqual(
      new Date('2/2/2020')
    );

    expect(interval.getNthOccurrenceOfDayForMonth(
      1, 6, new Date('2/1/2020')
    )).toEqual(
      new Date('2/1/2020')
    );

    expect(interval.getNthOccurrenceOfDayForMonth(
      3, 3, new Date('2/1/2020')
    )).toEqual(
      new Date('2/19/2020')
    );

    expect(interval.getNthOccurrenceOfDayForMonth(
      5, 6, new Date('2/1/2020')
    )).toEqual(
      new Date('2/29/2020')
    );
  });
});
