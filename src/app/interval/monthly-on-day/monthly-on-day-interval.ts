import { BaseInterval } from '../base-interval';
import { IntervalType } from '../types/interval-type.enum';

export class MonthlyOnDayInterval extends BaseInterval {
  constructor() {
    super(IntervalType.MonthlyOnNumberDay);
  }

  getNumOccurrences(
    dueDate: Date,
    startDate: Date,
    endDate: Date
  ): number {
    const dayOccArr = this.getDayAndOccurrenceForDate(dueDate);
    let firstOccurrence = this.getNthOccurrenceOfDayForMonth(dayOccArr[0], dayOccArr[1], startDate);
    let lastOccurrence = this.getNthOccurrenceOfDayForMonth(dayOccArr[0], dayOccArr[1], endDate);
    let occurrences = firstOccurrence.differenceInMonths(lastOccurrence, true);

    // If they're reversed, swap the first and last occurrence and start and end dates
    if (firstOccurrence.getTime() > lastOccurrence.getTime()) {
      occurrences *= -1;
      [firstOccurrence, lastOccurrence, startDate, endDate] =
        [lastOccurrence, firstOccurrence, endDate, startDate];
    }

    // If the first occurrence is before the start date
    if (firstOccurrence.getTime() < startDate.getTime()) {
      occurrences -= 1;
    }

    // If the last occurrence is after end date
    if (lastOccurrence.getTime() > endDate.getTime()) {
      occurrences -= 1;
    }

    return occurrences;
  }

  // Returns the Nth occurrence of the passed day for the month of the passed date
  // ex: The first sunday of February (1, 0, '1/2/2020') => 2/2/2020
  getNthOccurrenceOfDayForMonth(n: number, day: number, date: Date): Date {
    let dateOfOccurrence;
    const firstDateOfMonth = new Date(date);
    firstDateOfMonth.setFullYear(
      date.getUTCFullYear(),
      date.getMonth(),
      1
    );
    // Get difference in days between first of month and first desired day
    const dayDiff = (7 - firstDateOfMonth.getDay() + day) % 7;

    // Add difference + (n-1) weeks
    dateOfOccurrence = firstDateOfMonth.addDays(dayDiff + ((n - 1) * 7));

    return dateOfOccurrence;
  }

  // Returns the day of the week and the number it is for that months for the passed date
  // ex: 2/2/2020 => [1, 0] (first sunday)
  private getDayAndOccurrenceForDate(dueDate: Date): number[] {
    const day = dueDate.getDay();
    const date = dueDate.getDate();
    let mutableDate = date;
    let occurrence = 0;

    do {
      mutableDate -= 7;
      occurrence++;
    } while (mutableDate > 0);

    return [
      occurrence,
      day
    ];
  }
}
