export enum IntervalType {
  Daily = 1,
  WeeklyOnDay,
  MonthlyOnNumber,
  MonthlyOnNumberDay,
  Yearly
}
