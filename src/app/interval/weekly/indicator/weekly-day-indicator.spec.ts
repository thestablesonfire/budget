import { WeeklyDayIndicator } from './weekly-day-indicator';

describe('WeeklyDayIndicator', () => {
  it('should create an instance', () => {
    const indications = [false, true, true, true, true, true, false];

    expect(
      new WeeklyDayIndicator(
        indications,
        1
      )
    ).toBeTruthy();
  });

  it('should mark all days inactive', () => {
    const ind = new WeeklyDayIndicator([], 1);

    expect(testEachDay(ind)).toEqual([false, false, false, false, false, false, false]);
  });

  it('should mark all days active', () => {
    const allTrue = [true, true, true, true, true, true, true];
    const ind = new WeeklyDayIndicator(allTrue, 1);

    expect(testEachDay(ind)).toEqual(allTrue);
  });

  it('should mark selected days active', () => {
    const indications = [true, true, false, true, true, true, true];
    const ind = new WeeklyDayIndicator(indications, 1);

    expect(testEachDay(ind)).toEqual(indications);
  });

  function testEachDay(indicator: WeeklyDayIndicator): boolean[] {
    const days = [];
    days.length = 7;

    for (let i = 0; i < 7; i++) {
      days[i] = indicator.dayIsActive(i);
    }

    return days;
  }
});
