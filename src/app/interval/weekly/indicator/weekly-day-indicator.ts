import { WeeklyDays } from './weekly-days.enum';

export class WeeklyDayIndicator {
  constructor(
    private days: boolean[],
    private weekIteration: number,
  ) {
    if (this.days.length !== 7) {
      this.days.length = 7;
    }
  }

  dayIsActive(day: WeeklyDays): boolean {
    return !!this.days[day];
  }

  getDays(): boolean[] {
    return Array.from(this.days);
  }

  getIteration(): number {
    return this.weekIteration;
  }
}
