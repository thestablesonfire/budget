import { WeeklyDayIndicator } from './indicator/weekly-day-indicator';
import { WeeklyInterval } from './weekly-interval';

describe('WeeklyInterval', () => {
  let interval: WeeklyInterval;

  beforeEach(() => {
    interval = new WeeklyInterval(
      new WeeklyDayIndicator(
        [
          false,
          true,
          true,
          false,
          true,
          false,
          false,
        ],
        1
      )
    );
  });

  it('should create an instance', () => {
    expect(interval).toBeTruthy();
  });

  it('should calculate the correct number of occurrences', () => {
    expect(interval.getNumOccurrences(
      null,
      new Date('1/1/2020'),
      new Date('2/1/2020')
    )).toEqual(13);

    expect(interval.getNumOccurrences(
      null,
      new Date('1/2/2020'),
      new Date('1/23/2020')
    )).toEqual(10);

    expect(interval.getNumOccurrences(
      null,
      new Date('1/2/2020'),
      new Date('1/22/2020')
    )).toEqual(9);

    expect(interval.getNumOccurrences(
      null,
      new Date('1/3/2020'),
      new Date('1/23/2020')
    )).toEqual(9);
  });
});
