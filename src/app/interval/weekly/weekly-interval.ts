import { BaseInterval } from '../base-interval';
import { IntervalType } from '../types/interval-type.enum';
import { WeeklyDayIndicator } from './indicator/weekly-day-indicator';

export class WeeklyInterval extends BaseInterval {
  constructor(private indicator: WeeklyDayIndicator) {
    super(IntervalType.WeeklyOnDay);
  }

  getNumOccurrences(
    dueDate: Date,
    startDate: Date,
    endDate: Date
  ): number {
    const numWholeWeeks = startDate.differenceInWeeks(endDate);
    const numOccurrences = numWholeWeeks * this.getWeeklyOccurrences();
    const weekMultipliter = this.indicator.getIteration();
    let dayOfWeek = startDate.getDay();
    let numDaysDiff = startDate.differenceInDays(endDate, true) - (numWholeWeeks * 7 * weekMultipliter);
    let additionalOccurrences = 0;

    dueDate = null;

    while (numDaysDiff) {
      if (this.indicator.dayIsActive(dayOfWeek)) {
        additionalOccurrences++;
      }

      dayOfWeek = (dayOfWeek + 1) % (7 * weekMultipliter);
      numDaysDiff--;
    }

    return numOccurrences + additionalOccurrences;
  }

  private getWeeklyOccurrences(): number {
    return this.indicator.getDays().reduce((total, day) => {
      return day ? total + 1 : total;
    }, 0);
  }
}
