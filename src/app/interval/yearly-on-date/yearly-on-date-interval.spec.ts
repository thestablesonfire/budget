import { YearlyOnDateInterval } from './yearly-on-date-interval';

describe('YearlyOnDateInterval', () => {
  let interval: YearlyOnDateInterval;

  beforeEach(() => {
    interval = new YearlyOnDateInterval();
  });

  it('should create an instance', () => {
    expect(interval).toBeTruthy();
  });

  it('should correctly calculate num occurrences', () => {
    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2021')
    )).toEqual(2);

    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2020')
    )).toEqual(1);

    expect(interval.getNumOccurrences(
      new Date('3/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2021')
    )).toEqual(1);

    expect(interval.getNumOccurrences(
      new Date('6/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2023')
    )).toEqual(3);

    expect(interval.getNumOccurrences(
      new Date('6/1/2020'),
      new Date('1/1/2020'),
      new Date('1/1/2023')
    )).toEqual(3);

    expect(interval.getNumOccurrences(
      new Date('1/1/2020'),
      new Date('2/1/2020'),
      new Date('1/1/2024')
    )).toEqual(4);
  });
});
