import { BaseInterval } from '../base-interval';
import { IntervalType } from '../types/interval-type.enum';

export class YearlyOnDateInterval extends BaseInterval {
  constructor() {
    super(IntervalType.Yearly);
  }

  getNumOccurrences(
    dueDate: Date,
    startDate: Date,
    endDate: Date
  ): number {
    const dueMonth = dueDate.getMonth();
    const dueDayOfMonth = dueDate.getDate();
    let compareEndDate = new Date(endDate.valueOf());
    let compareStartDate = new Date(startDate.valueOf());
    let occurrences = 0;

    compareStartDate.setFullYear(
      startDate.getFullYear(),
      dueMonth,
      dueDayOfMonth
    );
    compareEndDate.setFullYear(
      endDate.getFullYear(),
      dueMonth,
      dueDayOfMonth
    );

    if (compareStartDate.getTime() < startDate.getTime()) {
      compareStartDate = compareStartDate.addYears(1);
    }

    if (compareEndDate.getTime() > endDate.getTime()) {
      compareEndDate = compareEndDate.addYears(-1);
    }

    while (compareStartDate.getTime() <= compareEndDate.getTime()) {
      occurrences++;
      compareStartDate = compareStartDate.addYears(1);
    }

    return occurrences;
  }
}
