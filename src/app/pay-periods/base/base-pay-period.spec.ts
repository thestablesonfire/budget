import { BaseExpense } from '../../expenses/expense/base/base-expense';
import { BaseInterval } from '../../interval/base-interval';
import { TransactionType } from '../../types/transaction-type.enum';
import { BasePayPeriod } from './base-pay-period';

describe('BasePayPeriod', () => {
  it('should create an instance', () => {
    expect(new BasePayPeriod(
      [],
      [],
      new Date('1/1/2019'),
      new Date('1/14/2019')
    )).toBeTruthy();
  });

  it('should test expense total', () => {
    const expenses = [
      new BaseExpense(
        'Coffee',
        5,
        new Date('1/1/2018'),
        new BaseInterval(1),
        TransactionType.Fixed,
      )
    ];

    const payPeriod = new BasePayPeriod(
      [],
      [],
      new Date('1/1/2018'),
      new Date('1/7/2018')
    );
    payPeriod.setExpenses(expenses);
    payPeriod.setIncomes([]);

    expect(payPeriod.getExpenseTotal()).toEqual(5);
  });
});
