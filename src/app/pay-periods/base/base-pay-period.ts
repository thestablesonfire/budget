import { BaseExpense } from 'src/app/expenses/expense/base/base-expense';
import { BaseIncome } from 'src/app/expenses/income/base/base-income';

export class BasePayPeriod {
  constructor(
    protected incomes: BaseIncome[],
    protected expenses: BaseExpense[],
    protected startDate: Date,
    protected endDate: Date
  ) { }

  getEndDate() {
    return this.endDate;
  }

  getExpenseTotal(): number {
    return this.getSumTotal(this.expenses);
  }

  getIncomeTotal(): number {
    return this.getSumTotal(this.incomes);
  }

  getStartDate() {
    return this.startDate;
  }

  setExpenses(expenses: BaseExpense[]) {
    this.expenses = expenses;
  }

  setIncomes(incomes: BaseIncome[]) {
    this.incomes = incomes;
  }

  private getSumTotal(property: any[]) {
    let sum = 0;

    property.forEach(item => {
      const amount = item.getExpensesForPeriod(this.startDate, this.endDate);

      sum += amount
        ? amount
        : 0;
    });

    return sum;
  }
}
