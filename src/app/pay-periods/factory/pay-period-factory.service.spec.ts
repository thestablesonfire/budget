import { TestBed } from '@angular/core/testing';

import { PayPeriodType } from '../../expenses/types/pay-period-type.enum';
import { PayPeriodFactoryService } from './pay-period-factory.service';

describe('PayPeriodFactoryService', () => {
  let service: PayPeriodFactoryService;

  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => {
    service = TestBed.get(PayPeriodFactoryService);

    jasmine.clock().install();
    jasmine.clock().mockDate(new Date('5/1/2020'));
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return valid weekly pay periods', () => {
    const periods = service.getPayPeriods(
      [],
      [],
      PayPeriodType.Weekly,
      new Date('1/1/2019'),
      3
    );

    expect(periods.length).toBe(3);
    expect(periods[0].getStartDate()).toEqual(new Date('5/5/2020'));
    expect(periods[0].getEndDate()).toEqual(new Date('5/11/2020'));
    expect(periods[1].getStartDate()).toEqual(new Date('5/12/2020'));
    expect(periods[1].getEndDate()).toEqual(new Date('5/18/2020'));
    expect(periods[2].getStartDate()).toEqual(new Date('5/19/2020'));
    expect(periods[2].getEndDate()).toEqual(new Date('5/25/2020'));
  });

  it('should return valid bi-weekly pay periods', () => {
    const periods = service.getPayPeriods(
      [],
      [],
      PayPeriodType.BiWeekly,
      new Date('1/1/2019'),
      3
    );

    expect(periods.length).toBe(3);
    expect(periods[0].getStartDate()).toEqual(new Date('5/12/2020'));
    expect(periods[0].getEndDate()).toEqual(new Date('5/25/2020'));
    expect(periods[1].getStartDate()).toEqual(new Date('5/26/2020'));
    expect(periods[1].getEndDate()).toEqual(new Date('6/8/2020'));
    expect(periods[2].getStartDate()).toEqual(new Date('6/9/2020'));
    expect(periods[2].getEndDate()).toEqual(new Date('6/22/2020'));
  });

  it('should return valid semi-monthly pay periods', () => {
    const periods = service.getPayPeriods(
      [],
      [],
      PayPeriodType.SemiMonthly,
      new Date('1/1/2019'),
      3
    );

    expect(periods.length).toBe(3);
    expect(periods[0].getStartDate()).toEqual(new Date('5/15/2020'));
    expect(periods[0].getEndDate()).toEqual(new Date('5/31/2020'));
    expect(periods[1].getStartDate()).toEqual(new Date('6/1/2020'));
    expect(periods[1].getEndDate()).toEqual(new Date('6/14/2020'));
    expect(periods[2].getStartDate()).toEqual(new Date('6/15/2020'));
    expect(periods[2].getEndDate()).toEqual(new Date('6/30/2020'));
  });

  it('should return valid monthly pay periods', () => {
    const periods = service.getPayPeriods(
      [],
      [],
      PayPeriodType.Monthly,
      new Date('1/1/2019'),
      3
    );

    expect(periods.length).toBe(3);
    expect(periods[0].getStartDate()).toEqual(new Date('6/1/2020'));
    expect(periods[0].getEndDate()).toEqual(new Date('6/30/2020'));
    expect(periods[1].getStartDate()).toEqual(new Date('7/1/2020'));
    expect(periods[1].getEndDate()).toEqual(new Date('7/31/2020'));
    expect(periods[2].getStartDate()).toEqual(new Date('8/1/2020'));
    expect(periods[2].getEndDate()).toEqual(new Date('8/31/2020'));
  });
});
