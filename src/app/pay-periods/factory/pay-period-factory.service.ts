import { BaseExpense } from 'src/app/expenses/expense/base/base-expense';

import { Injectable } from '@angular/core';

import { PayPeriodType } from '../../expenses/types/pay-period-type.enum';
import { BasePayPeriod } from '../base/base-pay-period';
import { BaseIncome } from 'src/app/expenses/income/base/base-income';

@Injectable({
  providedIn: 'root'
})
export class PayPeriodFactoryService {
  private periodBuilders: any[];

  constructor() {
    this.periodBuilders = [
      null,
      this.getWeeklyPayPeriod,
      this.getBiWeeklyPayPeriod,
      this.getSemiMonthlyPayPeriod,
      this.getMonthlyPayPeriod
    ];
  }

  getPayPeriods(
    incomes: BaseIncome[],
    expenses: BaseExpense[],
    type: PayPeriodType,
    seedDate: Date,
    periodsToMake: number
  ): BasePayPeriod[] {
    const payPeriods = [];
    let beginningDate = this.getMostCurrentStartDate(seedDate, type);

    for (let i = 0; i < periodsToMake; i++) {
      payPeriods.push(
        this.periodBuilders[type].call(this, incomes, expenses, new Date(beginningDate))
      );

      beginningDate = (new Date(payPeriods[i].getEndDate())).addDays(1);
    }

    return payPeriods;
  }

  private getMostCurrentStartDate(seedDate: Date, type: PayPeriodType): Date {
    const today = new Date();
    let diffInDays: number;

    switch (type) {
      case PayPeriodType.Weekly:
        diffInDays = seedDate.differenceInDays(today, true) % 7;
        break;
      case PayPeriodType.BiWeekly:
        diffInDays = seedDate.differenceInDays(today, true) % 14;
        break;
      case PayPeriodType.SemiMonthly:
        diffInDays = today.differenceInDays(this.getSemiMonthlyNextDate(), false);
        break;
      case PayPeriodType.Monthly:
        const nextDate = (new Date()).getLastDateOfMonth().addDays(1);

        diffInDays = today.differenceInDays(nextDate, false);
        break;
    }

    return today.addDays(diffInDays);
  }

  private getSemiMonthlyNextDate(): Date {
    const today = new Date();
    let nextDate: Date;

    if (today.getDate() < 15) {
      nextDate = new Date(today);
      nextDate.setFullYear(
        today.getFullYear(),
        today.getMonth(),
        15,
      );
    } else {
      nextDate = today.getLastDateOfMonth();
      nextDate = nextDate.addDays(1);
    }

    return nextDate;
  }

  private getWeeklyPayPeriod(
    incomes: BaseIncome[],
    expenses: BaseExpense[],
    startDate: Date
  ): BasePayPeriod {
    return this.getPayPeriod(incomes, expenses, startDate, this.weeklyFn);
  }

  private getBiWeeklyPayPeriod(
    incomes: BaseIncome[],
    expenses: BaseExpense[],
    startDate: Date
  ): BasePayPeriod {
    return this.getPayPeriod(incomes, expenses, startDate, this.biWeeklyFn);
  }

  private getSemiMonthlyPayPeriod(
    incomes: BaseIncome[],
    expenses: BaseExpense[],
    startDate: Date
  ): BasePayPeriod {
    return this.getPayPeriod(incomes, expenses, startDate, this.semiMonthlyFn);
  }

  private getMonthlyPayPeriod(
    incomes: BaseIncome[],
    expenses: BaseExpense[],
    startDate: Date
  ): BasePayPeriod {
    return this.getPayPeriod(incomes, expenses, startDate, this.monthlyFn);
  }

  private getPayPeriod(
    incomes: BaseIncome[],
    expenses: BaseExpense[],
    startDate: Date,
    endDateFn
  ) {
    const endDate = endDateFn(startDate);

    return new BasePayPeriod(incomes, expenses, startDate, endDate);
  }

  private weeklyFn(startDate: Date) {
    return startDate.addDays(6);
  }

  private biWeeklyFn(startDate: Date) {
    return startDate.addDays(13);
  }

  private semiMonthlyFn(startDate: Date) {
    const day = startDate.getDate();
    let endDate;

    if (day > 14) {
      startDate.setDate(15);
      endDate = startDate.getLastDateOfMonth();
    } else {
      startDate.setDate(1);
      endDate = new Date(startDate);
      endDate.setDate(14);
    }

    return endDate;
  }

  private monthlyFn(startDate: Date) {
    let endDate = new Date(startDate);

    endDate.setMonth(endDate.getMonth() + 1);
    endDate = endDate.addDays(-1);

    return endDate;
  }
}
