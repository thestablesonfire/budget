import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayPeriodsComponent } from './pay-periods.component';

describe('PayPeriodsComponent', () => {
  let component: PayPeriodsComponent;
  let fixture: ComponentFixture<PayPeriodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PayPeriodsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayPeriodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
