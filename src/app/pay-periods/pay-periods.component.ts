import { Component, OnInit } from '@angular/core';

import { BaseExpense } from '../expenses/expense/base/base-expense';
import { ExpenseService } from '../expenses/expense/services/expense.service';
import { PayPeriodType } from '../expenses/types/pay-period-type.enum';
import { BasePayPeriod } from './base/base-pay-period';
import { PayPeriodFactoryService } from './factory/pay-period-factory.service';
import { BaseIncome } from '../expenses/income/base/base-income';
import { IncomesService } from '../expenses/income/services/incomes.service';

@Component({
  selector: 'app-pay-periods',
  templateUrl: './pay-periods.component.html',
  styleUrls: ['./pay-periods.component.scss']
})
export class PayPeriodsComponent implements OnInit {
  public accountBalance: number;
  public expenses: BaseExpense[];
  public incomes: BaseIncome[];
  public payPeriods: BasePayPeriod[];

  constructor(
    private expenseService: ExpenseService,
    private incomesService: IncomesService,
    private payPeriodFactory: PayPeriodFactoryService
  ) {
    this.accountBalance = 687.30;
  }

  ngOnInit() {
    this.getIncomes();
    this.getExpenses();
    this.getPayPeriods();
  }

  getFinalAmount(period: BasePayPeriod): string {
    const incomeTotal = period.getIncomeTotal();
    const expenseTotal = period.getExpenseTotal();
    console.log(incomeTotal, expenseTotal);

    return (this.accountBalance + period.getIncomeTotal() - period.getExpenseTotal()).toFixed(2);
  }

  private getExpenses() {
    this.expenses = this.expenseService.getExpenses();
  }

  private getIncomes() {
    this.incomes = this.incomesService.getIncomes();
  }

  private getPayPeriods() {
    this.payPeriods = this.payPeriodFactory.getPayPeriods(
      this.incomes,
      this.expenses,
      PayPeriodType.BiWeekly,
      new Date('5/8/2020'),
      3
    );
  }
}
