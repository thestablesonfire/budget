import { TestBed } from '@angular/core/testing';

import { PayPeriodService } from './pay-period.service';

describe('PayPeriodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PayPeriodService = TestBed.get(PayPeriodService);
    expect(service).toBeTruthy();
  });
});
