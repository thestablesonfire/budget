export enum TransactionType {
  Fixed = 1,
  Variable,
  Intermittent
}
